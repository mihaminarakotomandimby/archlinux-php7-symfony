FROM archlinux:latest
RUN pacman -Sy      \
    --noconfirm     \
    php7            \ 
    php7-apache     \
    apache          \
    mariadb-clients wget


RUN rm -rfv /etc/httpd && mkdir -pv /etc/httpd
ADD ./httpd /etc/httpd/
RUN find /etc/httpd

RUN rm -rfv /etc/php7 && mkdir -pv /etc/php7
ADD ./php7 /etc/php7
RUN find /etc/php7

RUN mkdir -pv /composer/bin
RUN wget https://getcomposer.org/download/2.0.11/composer.phar -O /composer/bin/composer
RUN chmod 755 /composer/bin/composer
RUN ln -sv /usr/bin/php7 /usr/bin/php 

ENV PATH="/composer/bin:"${PATH}

RUN mkdir -pv /srv/http/
ADD ./code    /srv/http/

WORKDIR /srv/http
RUN composer config extra.symfony.allow-contrib true
# --no-interaction
RUN composer install --no-interaction
RUN composer update --no-interaction


EXPOSE 80
ADD ./entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh 
RUN chown -R http.http /srv/http/openflex-cloud

ENTRYPOINT  ["/entrypoint.sh"]
